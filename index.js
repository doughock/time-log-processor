const fs = require('fs')
const path = require('path')
const colors = require('colors')
const { program } = require('commander')

program
  .option('-d, --debug', 'output extra debugging', false)
  .option('-p, --project', 'summarize result by project', false)
  .option('-u, --include_unbillable', 'include unbillable time', false)
  .option('-i, --input [files...]', 'space separated list of files', [])

program.parse(process.argv)
const options = program.opts()

const DEBUG = (process.env.DEBUG === 'true') || options.debug

let projects = new Map()
let entries = []
let dailyHours = []
let issues = []
let unbillableMins = 0

// Expected file format
// YYYY-MM-DD
// HH:MM-HH:MM  Description of activity

const processTimeLogs = async ({ files, groupByProject }) => {
  try {
    for (const filePath of files) {
      let data = fs.readFileSync(filePath)
      let entries = await processData(data)
      let { entries: calculatedTimeEntries, projects } = await calculateTimeEntries({ entries, groupByProject })
      calculateDailyHours(entries)
      if (groupByProject) {
        printProjectSummary(projects)
        outputDailyCSV({ filePath, projects })
      }
    }
  } catch (err) {
    console.log('ERROR'.red, err)
  }
}

const processData = async (data) => {
  let currentDay
  let array = data.toString().split('\n')
  DEBUG && console.log(array)

  array.forEach((entry) => {
    let ignore = false

    if (entry.trim() !== '') {
      let line = entry.match(/(\d+)\-(\d+)\-(\d+)/)
      if(line) {
        DEBUG && console.log(line[1], line[2], line[3])
        currentDay = entry
        DEBUG && console.log('currentDay'.cyan, currentDay)
      } else {
        if (entry.match(/^I/)) {
          //line = entry.match(/^[I ]+(\d+:\d+)-(\d+:\d+)[\t\r\n\f ]+([a-zA-Z0-9 -\/\[\],()]+)/)
          ignore = true
          //DEBUG && console.log('IGNORED'.red, Array.isArray(line) ? line[0] : line)
          DEBUG && console.log('IGNORED'.red, entry)
          //return
        }
        //line = entry.match(/(\d+:\d+)-(\d+:\d+)[\t\r\n\f ]+([a-zA-Z0-9 -\/\[\],()]+)/)
        line = entry.match(/^[I ]*(\d+:\d+)-(\d+:\d+)[\t\r\n\f ]+([a-zA-Z0-9 -\/\[\],()]+)/)
        DEBUG && console.log('line'.inverse, line)

        if(line) {
          DEBUG && console.log('start', line[1])
          DEBUG && console.log('  end', line[2])
          DEBUG && console.log(' desc', line[3])
        } else {
          console.log('Invalid format'.red, `[${entry}]`)
          return
        }

        entries.push({ 
          ignore,
          date: currentDay,
          start: line[1],
          stop: line[2],
          description: line[3]
        })
      }   
    }
  })

  DEBUG && console.log(entries)
  return entries
}

const calculateTimeEntries = async ({ entries, groupByProject }) => {
  entries.forEach((entry) => {
    DEBUG && console.log(`   day: ${entry.date}`)
    DEBUG && console.log(` start: ${entry.start}`)
    DEBUG && console.log(`  stop: ${entry.stop}`)
    DEBUG && console.log(`  desc: ${entry.description}`)
    DEBUG && console.log(`ignore: ${entry.ignore}`)

    let y = entry.date.split('-')[0]
    let m = entry.date.split('-')[1] - 1
    let d = entry.date.split('-')[2]

    let sh = entry.start.split(':')[0]
    let sm = entry.start.split(':')[1]

    let eh = entry.stop.split(':')[0]
    let em = entry.stop.split(':')[1]

    let d1 = new Date(y, m, d, sh, sm)
    let d2 = new Date(y, m, d, eh, em)

    DEBUG && console.log(d1)
    DEBUG && console.log(d2)

    let diff = d2 - d1
    DEBUG && console.log('Entry Diff (min)', (diff/1000)/60)

    let hours = Math.floor(diff / 1000 / 60 / 60)
    diff -= hours * 1000 * 60 * 60
    let minutes = Math.floor(diff / 1000 / 60)

    DEBUG && console.log(`    h: ${hours} m: ${minutes}`)

    if (hours < 0) {
      console.log(`POSSIBLE CALCULATION ISSUE ${hours}`.red)
      issues.push({
        entry,
        issue: `POSSIBLE CALCULATION ISSUE ${hours}`
      })
    }

    entry.hours = hours
    entry.min = minutes

    // add to projects
    if (!entry.ignore && groupByProject) {
      let timeMins = ((d2 - d1)/1000)/60
      let parts = entry.description.split(' - ')
      let project = parts[0]
      let owner = (parts.length === 3) ? parts[1] : 'unknown'
      let details = (parts.length === 3) ? parts[2] : parts[1]

      DEBUG && console.log('DATE', entry.date, 'TIME', timeMins, 'PROJECT', project, 'OWNER', owner, 'DETAILS', details, `${(entry.ignore) ? 'IGNORE' : ''}`)

      if (projects.has(project)) {
        let data = projects.get(project)
        data.timeMins += timeMins
        data.timeHrsMins = getTimeHrsMins(data.timeMins)
        data.entries.push({
          date: entry.date,
          time: timeMins,
          project,
          owner,
          details
        })
        projects.set(project, data)
      } else {
        let data = {}
        data.timeMins = timeMins
        data.timeHrsMins = getTimeHrsMins(data.timeMins)
        data.entries = [{
          date: entry.date,
          time: timeMins,
          project,
          owner,
          details
        }]
        projects.set(project, data)
      }  
    }
  })

  return {
    entries,
    projects
  }
}

// get time in hours and minutes as string
const getTimeHrsMins = (timeMins) => {
  return `${timeMins/60}  OR  ${Math.floor(timeMins/60)} Hrs ${timeMins % 60} Mins`
}

// date, mins, owner, project, details
const outputDailyCSV = ({ filePath, projects }) => {
  const outputPath = path.join('./', `${path.parse(filePath).name}.csv`)

  const keys = Array.from(projects.keys()).sort()

  data = '"DATE","TIME (MINS)","OWNER","PROJECT","DESCRIPTION"\n'

  for (key of keys) {
    const entries = projects.get(key).entries

    data += entries
      .filter( e => !e.ignore)
      .reduce((accum, e) => {
        return accum += `"${e.date}","${e.time}","${e.owner}","${e.project}","${(e.details !== undefined) ? e.details : ''}"\n`
      }, '')
  }

  fs.writeFileSync(outputPath, data, 'utf-8')
  console.log(`Wrote ${outputPath} (records ${data.length})`.green)
}

const calculateDailyHours = async (entries) => {
  let current

  entries.forEach((entry) => {
    if (!dailyHours[entry.date]) {
      dailyHours[entry.date] = { hours: 0, minutes: 0 }
    }

    DEBUG && console.log({ entry })

    if (!entry.ignore || (entry.ignore && options.include_unbillable === true)) {
      dailyHours[entry.date].hours += entry.hours
      dailyHours[entry.date].minutes += entry.min

      if (dailyHours[entry.date].minutes >= 60) {
        dailyHours[entry.date].minutes -= 60
        dailyHours[entry.date].hours++
      }
    } else {
      unbillableMins += (entry.hours * 60) + entry.min
    }
  })

  console.log(`Unbillable minutes: ${unbillableMins}`)
  printRecords(dailyHours)
  total(dailyHours)
}

const printRecords = (dailyHours) => {
  console.log('\nDAILY HOURS'.bgBlue.bold)
  for (var key in dailyHours) {
    if (dailyHours[key].hours > 12) {
      console.log(key.bgRed, dailyHours[key])
    } else if (dailyHours[key].hours > 8) {
      console.log(key.bgYellow, dailyHours[key])
    } else {
      console.log(key, dailyHours[key])
    }
  }
}

const total = (dailyHours) => {
  let hours = 0
  let minutes = 0

  for (var key in dailyHours) {
    hours += dailyHours[key].hours
    minutes += dailyHours[key].minutes

    if (minutes >= 60) {
      hours += 1
      minutes -= 60
    }
    DEBUG && console.log(key + ' TOTAL hours', dailyHours[key].hours, 'minutes', dailyHours[key].minutes)
  }

  issues.forEach( item => {
    console.log('ISSUE'.red, item)
  })

  // total billable or total billable + total unbillable (depends on -u option)
  console.log('           TOTAL'.green.bold, 'hours:', `${hours}`.magenta, ' minutes:', `${minutes}`.magenta)

  // total unbillable (depends on -u option)
  const totalUnbillableHours = Math.floor(unbillableMins / 60)
  const totalUnbillableMins = unbillableMins - (totalUnbillableHours * 60)
  console.log('UNBILLABLE TOTAL'.green.bold, 'hours:', `${totalUnbillableHours}`.magenta, ' minutes:', `${totalUnbillableMins}`.magenta)
}

const printProjectSummary =  (projects) => {
  console.log('\nPROJECT SUMMARY'.bgBlue.bold)

  // calculate the longest project name
  const maxLength = Array.from(projects.keys()).sort((a, b) => { return b.length - a.length })[0].length + 2

  // get project names in alphabetic order
  const projectNames = Array.from(projects.keys()).sort()

  // iterate of projects and print details
  for (const key of projectNames) {
    const project = projects.get(key)
    console.log(key.padEnd(maxLength).cyan, `${project.timeMins.toString().padStart(4)}`.magenta, ` mins [`, `${project.timeHrsMins.padEnd(26)}`.magenta, `] Number of Entries: `, `${project.entries.length}`.magenta)
  }

}

if (options.input.length > 0) {
  processTimeLogs({ files: options.input, groupByProject: options.project })
} else {
  program.help()
}
