# Time Log Processor

Node program to parse the time log files that I use to track my time.

## How to run
```bash
// directly run
node index.js -i <path/to/file>

// print project grouping totals
node index.js -i <path/to/file> -p
```

## Extract Support Only Charges
```
// print out support line items
cat lemieux-2021-02.txt | egrep 'Support|2021-02' > support.txt
```

## Expected Time Log File Format - examples

Prepend line with 'I' to mark as unbillable

```
YYYY-MM-DD
HH:MM-HH:MM  <PROJECT> - <STAKEHOLDER> - <DESCRIPTION OF ACTIVITY>
HH:MM-HH:MM  IT wkly meeting
HH:MM-HH:MM  INVOICE

YYYY-MM-DD
HH:MM-HH:MM  <PROJECT> - dhock - <DESCRIPTION OF ACTIVITY>
I HH:MM-HH:MM  <PROJECT> - <STAKEHOLDER> - <DESCRIPTION OF ACTIVITY>
```
